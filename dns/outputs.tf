output "zone_ids" {
  value = "${map(
      "mattbot_net", "${aws_route53_zone.mattbot_net.id}"
    )}"
}

output "name_servers" {
  value = "${aws_route53_delegation_set.main.name_servers}"
}
