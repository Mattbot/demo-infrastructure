resource "aws_route53_zone" "mattbot_net" {
  name              = "mattbot.net"
  delegation_set_id = "${aws_route53_delegation_set.main.id}"

  tags {
    ProductGroup = "mattbot.net"
  }
}

# resource "aws_route53_record" "mattbot_net_bastion_A" {
#   name    = "bastion"
#   zone_id = "${aws_route53_zone.mattbot_net.zone_id}"
#   type    = "A"
#   ttl     = "180"
#   records = ["0.0.0.0"]
# }
