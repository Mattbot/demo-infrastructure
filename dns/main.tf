terraform {
  backend "s3" {
    bucket = "net.mattbot.demo.terraform-state"
    key    = "dns.tfstate"
  }
}

resource "aws_route53_delegation_set" "main" {
  reference_name = "main"
}
