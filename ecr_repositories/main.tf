provider "aws" {
  region  = "us-east-1"
  version = "1.18"
}

terraform {
  backend "s3" {
    bucket = "net.mattbot.demo.terraform-state"
    key    = "ecr_repositories.tfstate"
  }
}

resource "aws_ecr_repository" "demo_static_www" {
  name = "demo-static-www"
}

output "demo_static_www" {
  value = "${replace(aws_ecr_repository.demo_static_www.repository_url, "https://", "")}"
}

resource "aws_ecr_repository" "cisco_urlinfo_service" {
  name = "cisco-urlinfo-service"
}

output "cisco_urlinfo_service" {
  value = "${replace(aws_ecr_repository.cisco_urlinfo_service.repository_url, "https://", "")}"
}

resource "aws_ecr_repository" "demo_flux_client" {
  name = "demo-flux-client"
}

output "demo_flux_client" {
  value = "${replace(aws_ecr_repository.demo_flux_client.repository_url, "https://", "")}"
}

resource "aws_ecr_repository" "demo_jwt_api" {
  name = "demo-jwt-api"
}

output "demo_jwt_api" {
  value = "${replace(aws_ecr_repository.demo_jwt_api.repository_url, "https://", "")}"
}

resource "aws_ecr_repository" "demo_blog_api" {
  name = "demo-blog-api"
}

output "demo_blog_api" {
  value = "${replace(aws_ecr_repository.demo_blog_api.repository_url, "https://", "")}"
}
