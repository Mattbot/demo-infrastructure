# nb: this is structured this way because: any map in TF must have values with all the same type!

# all values which are tied to either a VPC or an ECS cluster should be indexed by only the ecs-cluster name, so an app can specify just that name and receive the correct VPC info without having to have the correct VPC ID first :-/

output "dns_zone_id" {
  value = "${aws_route53_zone.domain.id}"
}

output "dns_zone_name" {
  value = "${aws_route53_record.parent_NS.fqdn}"
}

output "subnets" {
  # nb: join needed on this value so lookup() can be used to extract it :-/
  value = "${map(
                "vpc0", "${join(",", module.vpc0.subnets)}"
              )}"
}

output "vpc_cidr" {
  value = "${map(
                "vpc0", "${module.vpc0.vpc_cidr}"
              )}"
}

output "vpc_id" {
  value = "${map(
                "vpc0", "${module.vpc0.vpc_id}"
              )}"
}
