terraform {
  backend "s3" {
    bucket = "net.mattbot.demo.terraform-state"
    key    = "environments/preproduction.tfstate"
  }
}

provider "aws" {
  region  = "${var.region}"
  version = "0.1"
}

provider "ignition" {
  version = "1.0"
}

provider "template" {
  version = "1.0"
}

data "aws_availability_zones" "available" {
  state = "available"
}

data "terraform_remote_state" "dns" {
  backend = "s3"

  config {
    bucket = "net.mattbot.demo.terraform-state"
    key    = "dns.tfstate"
  }
}

module "vpc0" {
  source = "../../lib/vpc"

  environment = "${var.environment}"
  name        = "vpc0"
  region      = "${var.region}"

  availability_zones = "${slice(data.aws_availability_zones.available.names, 1, length(data.aws_availability_zones.available.names))}"
  public_dns_zone_id = "${data.terraform_remote_state.dns.zone_ids["mattbot_net"]}"

  bastion_users = "${var.bastion_users}"
}

resource "aws_route53_zone" "domain" {
  name = "${var.environment}.mattbot.net." # preproduction.mattbot.net

  tags {
    Paradigm    = "demo"
    Environment = "${var.environment}"
  }
}

resource "aws_route53_record" "parent_NS" {
  name    = "${var.environment}"
  type    = "NS"
  zone_id = "${data.terraform_remote_state.dns.zone_ids["mattbot_net"]}"
  records = ["${aws_route53_zone.domain.name_servers}"]
  ttl     = "180"
}
