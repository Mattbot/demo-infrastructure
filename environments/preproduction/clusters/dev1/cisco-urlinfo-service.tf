# module "cisco-urlinfo-service" {
#   source = "../../../../apps/cisco-urlinfo-service"

#   cluster_dns_zone_id      = "${module.cluster.dns_zone_id}"
#   cluster_name             = "${var.cluster_name}"
#   deployment_name          = "dungeon-diplomacy"
#   deployment_purpose       = "development"
#   docker_image_repo        = "${data.terraform_remote_state.ecr_repositories.cisco_urlinfo_service}"
#   docker_image_tag         = "${var.app_docker_image_tags["cisco-urlinfo-service"]}"
#   ecs_cluster_id           = "${module.cluster.ecs_cluster_id}"
#   elb_security_groups      = ["${aws_security_group.elb.id}"]
#   elb_subnets              = ["${split(",", lookup(data.terraform_remote_state.environment.subnets, var.vpc_name_from_environment))}"]
#   desired_count            = "1"
#   min_count                = "1"
#   max_count                = "1"
#   environment              = "${var.environment}"
#   # notification_target_arns = ["${data.terraform_remote_state.global.slack_notify_sns_topic}"]
#   project_name             = "cisco-urlinfo-service"
#   public_dns_zone_id       = "${data.terraform_remote_state.dns.zone_ids["mattbot_net"]}"
#   region                   = "${var.region}"
#   vpc_id                   = "${lookup(data.terraform_remote_state.environment.vpc_id, var.vpc_name_from_environment)}"

#   database_url             = "mysql2://${aws_db_instance.mysql-demo.username}:${aws_db_instance.mysql-demo.password}@${aws_db_instance.mysql-demo.endpoint}/cisco-dungeon-diplomacy?pool=32"
#   hmac_secret              = "secret_squirrel"
# }

# resource "aws_route53_record" "cisco-urlinfo-service" {
#   name    = "cisco-urlinfo-service"
#   records = ["${module.cisco-urlinfo-service.fqdn}"]
#   ttl     = 180
#   type    = "CNAME"
#   zone_id = "${data.terraform_remote_state.dns.zone_ids["mattbot_net"]}"
# }

# resource "aws_route53_record" "cisco" {
#   name    = "cisco"
#   records = ["${module.cisco-urlinfo-service.fqdn}"]
#   ttl     = 180
#   type    = "CNAME"
#   zone_id = "${data.terraform_remote_state.dns.zone_ids["mattbot_net"]}"
# }
