variable "region" {
  type        = "string"
  description = "AWS Region to use -- note that this is used *instead of* the default configured region in other AWS tooling"
}

variable "cluster_name" {
  type        = "string"
  description = "desired name of the 'cluster' to build"
}

variable "environment" {}

variable "vpc_name_from_environment" {
  description = "Logical name of the VPC defined in the environment, in which to build the cluster"
  default     = "vpc0"
}

variable "ssh_ingress_cidr" {
  default = "10.0.0.0/16" # IP of bastion host
}

variable "app_docker_image_tags" {
  description = "Map[logical_app_name]{docker_image_tag}"

  default = {
    demo-static-www         = "b06961e62bc31d51700ae0e26c8de6e71a7dd95c"
    cisco-urlinfo-service   = "112e0dd1cadf0053d33658f0389842c249d5435b"
  }
}
