# resource "aws_sns_topic" "ops_activity" {
#   name = "${var.environment}-${var.cluster_name}-ops-activity"
# }

# resource "aws_sns_topic_subscription" "ops_activity" {
#   endpoint               = "https://mattbot-sns-slack-gateway.herokuapp.com/channel/ops-activity"
#   endpoint_auto_confirms = true
#   protocol               = "https"
#   topic_arn              = "${aws_sns_topic.ops_activity.arn}"
# }

resource "aws_db_subnet_group" "main" {
  name       = "main"
  subnet_ids = ["${split(",", lookup(data.terraform_remote_state.environment.subnets, var.vpc_name_from_environment))}"]

  tags {
    Name = "${var.cluster_name}-db"
  }
}

# resource "aws_elasticache_subnet_group" "primary" {
#   name       = "${var.cluster_name}"
#   subnet_ids = ["${split(",", lookup(data.terraform_remote_state.environment.subnets, var.vpc_name_from_environment))}"]
# }

# resource "aws_security_group" "memcached" {
#   vpc_id = "${lookup(data.terraform_remote_state.environment.vpc_id, var.vpc_name_from_environment)}"

#   ingress {
#     from_port       = 11211
#     protocol        = 6
#     security_groups = ["${aws_security_group.ecs.id}"]
#     to_port         = 11211
#   }

#   egress {
#     cidr_blocks = ["0.0.0.0/0"]
#     from_port   = 0
#     protocol    = "-1"
#     to_port     = 0
#   }

#   tags {
#     Cluster     = "${var.cluster_name}"
#     Environment = "${var.environment}"
#     Name        = "${var.cluster_name}-memcached"
#     Paradigm    = "nova"
#   }
# }

# # A generic, shared, memcached instance for use by any apps which need Memcache as a LRU cache of non-sensitive data
# resource "aws_elasticache_cluster" "memcached_shared" {
#   apply_immediately      = true
#   az_mode                = "cross-az"
#   cluster_id             = "${var.cluster_name}-shared-m"
#   engine                 = "memcached"
#   engine_version         = "1.4.24"
#   node_type              = "cache.t2.micro"
#   notification_topic_arn = "${aws_sns_topic.ops_activity.arn}"
#   num_cache_nodes        = 3
#   parameter_group_name   = "${aws_elasticache_parameter_group.memcached_shared.name}"
#   port                   = 11211
#   security_group_ids     = ["${aws_security_group.memcached.id}"]
#   subnet_group_name      = "${aws_elasticache_subnet_group.primary.name}"

#   tags {
#     Cluster     = "${var.cluster_name}"
#     Environment = "${var.environment}"
#     Name        = "${var.cluster_name}-memcached"
#     Paradigm    = "demo"
#   }
# }

# resource "aws_elasticache_parameter_group" "memcached_shared" {
#   family = "memcached1.4"
#   name   = "${var.cluster_name}-shared-m"

#   # for a shared memcache cluster, we don't want to enable flush_all
#   parameter {
#     name  = "disable_flush_all"
#     value = "1"
#   }
# }

resource "aws_db_instance" "mysql-demo" {
  allocated_storage         = 10
  engine                    = "mysql"
  engine_version            = "5.7.19"
  instance_class            = "db.t2.small"
  username                  = "db_user"
  password                  = "${sha1("${var.cluster_name}")}"
  vpc_security_group_ids    = ["${aws_security_group.mysql.id}"]
  db_subnet_group_name      = "${aws_db_subnet_group.main.name}"
  parameter_group_name      = "default.mysql5.6"
  storage_encrypted         = false
  apply_immediately         = true
  storage_type              = "gp2"
  final_snapshot_identifier = "${var.cluster_name}-mysql-${uuid()}"
  skip_final_snapshot       = false
  copy_tags_to_snapshot     = true
  multi_az                  = true
  parameter_group_name      = "${aws_db_parameter_group.mysql.name}"

  tags {
    Cluster     = "${var.cluster_name}"
    Environment = "${var.environment}"
    Name        = "${var.cluster_name}-mysql"
    Paradigm    = "demo"
  }

  lifecycle {
    prevent_destroy = true
    ignore_changes  = ["final_snapshot_identifier"]
  }
}

resource "aws_db_parameter_group" "mysql" {
  name   = "${var.cluster_name}-mysql"
  family = "mysql5.7"

  parameter {
    name  = "character_set_server"
    value = "utf8"
  }

  parameter {
    name  = "character_set_client"
    value = "utf8"
  }
}

resource "aws_security_group" "mysql" {
  vpc_id = "${lookup(data.terraform_remote_state.environment.vpc_id, var.vpc_name_from_environment)}"

  ingress {
    from_port       = 3306
    protocol        = 6
    security_groups = ["${aws_security_group.ecs.id}"]
    to_port         = 3306
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
  }

  tags {
    Cluster     = "${var.cluster_name}"
    Environment = "${var.environment}"
    Name        = "${var.cluster_name}-mysql"
    Paradigm    = "demo"
  }
}
