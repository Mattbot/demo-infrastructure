module "demo-static-www" {
  source = "../../../../apps/demo-static-www"

  cluster_dns_zone_id      = "${module.cluster.dns_zone_id}"
  cluster_name             = "${var.cluster_name}"
  deployment_name          = "annoying-romance"
  deployment_purpose       = "development"
  docker_image_repo        = "${data.terraform_remote_state.ecr_repositories.demo_static_www}"
  docker_image_tag         = "${var.app_docker_image_tags["demo-static-www"]}"
  ecs_cluster_id           = "${module.cluster.ecs_cluster_id}"
  elb_security_groups      = ["${aws_security_group.elb.id}"]
  elb_subnets              = ["${split(",", lookup(data.terraform_remote_state.environment.subnets, var.vpc_name_from_environment))}"]
  desired_count            = "1"
  min_count                = "1"
  max_count                = "1"
  environment              = "${var.environment}"
  # notification_target_arns = ["${data.terraform_remote_state.global.slack_notify_sns_topic}"]
  project_name             = "demo-static-www"
  public_dns_zone_id       = "${data.terraform_remote_state.dns.zone_ids["mattbot_net"]}"
  region                   = "${var.region}"
  vpc_id                   = "${lookup(data.terraform_remote_state.environment.vpc_id, var.vpc_name_from_environment)}"

  hugo_append_port         = "false"
  hugo_base_url            = "https://resume.mattbot.net"
  hugo_port                = "3000"
}

resource "aws_route53_record" "demo-static-www" {
  name    = "demo-static-www"
  records = ["${module.demo-static-www.fqdn}"]
  ttl     = 180
  type    = "CNAME"
  zone_id = "${data.terraform_remote_state.dns.zone_ids["mattbot_net"]}"
}

resource "aws_route53_record" "resume" {
  name    = "resume"
  records = ["${module.demo-static-www.fqdn}"]
  ttl     = 180
  type    = "CNAME"
  zone_id = "${data.terraform_remote_state.dns.zone_ids["mattbot_net"]}"
}

