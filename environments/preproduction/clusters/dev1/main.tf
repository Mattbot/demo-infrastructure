terraform {
  backend "s3" {
    bucket = "net.mattbot.demo.terraform-state"
    key    = "environments/preproduction/clusters/dev1.tfstate"
  }
}

provider "aws" {
  region  = "${var.region}"
  version = "0.1"
}

provider "template" {
  version = "0.1"
}

data "terraform_remote_state" "environment" {
  backend = "s3"

  config {
    bucket = "net.mattbot.demo.terraform-state"
    key    = "environments/${var.environment}.tfstate"
  }
}

data "terraform_remote_state" "dns" {
  backend = "s3"

  config {
    bucket = "net.mattbot.demo.terraform-state"
    key    = "dns.tfstate"
  }
}

data "terraform_remote_state" "ecr_repositories" {
  backend = "s3"

  config {
    bucket = "net.mattbot.demo.terraform-state"
    key    = "ecr_repositories.tfstate"
  }
}

data "terraform_remote_state" "global" {
  backend = "s3"

  config {
    bucket = "net.mattbot.demo.terraform-state"
    key    = "global.tfstate"
  }
}

module "cluster" {
  source = "../../../../lib/cluster"

  dns_zone_id                  = "${data.terraform_remote_state.environment.dns_zone_id}"
  dns_zone_name                = "${data.terraform_remote_state.environment.dns_zone_name}"
  ecs_subnets                  = ["${split(",", lookup(data.terraform_remote_state.environment.subnets, var.vpc_name_from_environment))}"]
  ecs_instance_security_groups = ["${aws_security_group.ecs.id}"]
  environment                  = "${var.environment}"
  instance_type                = "t2.micro"
  desired_capacity             = "1"
  min_size                     = "1"
  max_size                     = "2"
  name                         = "${var.cluster_name}"
  region                       = "${var.region}"
  ssh_key_name                 = "demo"
  vpc_id                       = "${lookup(data.terraform_remote_state.environment.vpc_id, var.vpc_name_from_environment)}"
}
