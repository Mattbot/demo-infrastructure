resource "aws_security_group" "ecs" {
  vpc_id = "${lookup(data.terraform_remote_state.environment.vpc_id, var.vpc_name_from_environment)}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.ssh_ingress_cidr}"]
  }

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    self      = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = ["${aws_security_group.elb.id}"]
  }

  tags {
    Cluster     = "${var.cluster_name}"
    Environment = "${var.environment}"
    Name        = "ecs-instance"
    Paradigm    = "demo"
  }
}

resource "aws_security_group" "elb" {
  vpc_id = "${lookup(data.terraform_remote_state.environment.vpc_id, var.vpc_name_from_environment)}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Cluster     = "${var.cluster_name}"
    Environment = "${var.environment}"
    Name        = "elb-default"
    Paradigm    = "demo"
  }
}
