variable "region" {
  description = "AWS Region"
}

variable "environment" {
  description = "name of the environment"
}

variable "name" {
  description = "Name of the nova cluster (also used as name of the ECS cluster, for domain names, etc."
}

variable "dns_zone_id" {
  description = "ID of the mattbot.net DNS zone into which the #{name}.mattbot.net Route53 zone will be defined"
}

variable "dns_zone_name" {
  description = "Name of the mattbot.net DNS zone into which the #{name}.mattbot.net Route53 zone will be defined"
}

variable "vpc_id" {
  description = "ID of the VPC in which to launch ECS instances"
}

variable "instance_type" {
  description = "EC2 instance type for the ECS instances"
  default     = "t2.large"
}

variable "ssh_key_name" {
  description = "name of preexisting EC2 SSH key"
  default     = "code"
}

variable "ecs_subnets" {
  type = "list"
}

variable "min_size" {
  default = "1"
}

variable "max_size" {
  default = "10"
}

variable "desired_capacity" {
  default = "3"
}

variable "ecs_instance_security_groups" {
  type = "list"
}
