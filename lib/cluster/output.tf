output "ecs_cluster_id" {
  value = "${module.ecs.ecs_cluster_id}"
}

output "dns_zone_id" {
  value = "${aws_route53_zone.domain.id}"
}
