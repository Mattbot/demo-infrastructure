module "ecs" {
  source = "../ecs"

  desired_capacity             = "${var.desired_capacity}"
  domain_name                  = "${aws_route53_record.parent_NS.fqdn}"
  ecs_instance_security_groups = "${var.ecs_instance_security_groups}"
  ecs_subnets                  = "${var.ecs_subnets}"
  environment                  = "${var.environment}"
  max_size                     = "${var.max_size}"
  min_size                     = "${var.min_size}"
  name                         = "${var.name}"
  ssh_key_name                 = "${var.ssh_key_name}"
  ecs_instance_type            = "${var.instance_type}"
}

resource "aws_route53_zone" "domain" {
  name = "${var.name}.${var.dns_zone_name}"

  tags {
    Paradigm    = "demo"
    Cluster     = "${var.name}"
    Environment = "${var.environment}"
  }
}

resource "aws_route53_record" "parent_NS" {
  name    = "${var.name}"
  type    = "NS"
  zone_id = "${var.dns_zone_id}"
  records = ["${aws_route53_zone.domain.name_servers}"]
  ttl     = "180"
}
