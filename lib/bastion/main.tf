module "bastion" {
  source = "../../lib/ec2"

  environment             = "${var.environment}"
  name                    = "bastion"
  ami                     = "ami-9e2685e3" #coreos hvm
  instance_type           = "t2.micro"
  subnet_id               = "${var.subnet_id}"
  vpc_security_group_ids  = ["${aws_security_group.ssh-bastion.id}"]
  key_name                = "demo"
  user_data               = "${data.ignition_config.bastion.rendered}"
}

resource "aws_cloudwatch_log_group" "ssh-bastion" {
  name              = "/environments/${var.environment}/ssh-bastion"
  retention_in_days = 30
}

resource "aws_route53_record" "canonical_hostname" {
  zone_id = "${var.public_dns_zone_id}"
  name    = "${var.environment}-bastion"
  type    = "A"
  ttl     = "180"
  records = ["${module.bastion.public_ip}"]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "ssh-skip-proxy" {
  zone_id = "${var.public_dns_zone_id}"
  name    = "${var.environment}-bastion-direct"
  type    = "A"
  ttl     = "180"
  records = ["${module.bastion.public_ip}"]

  lifecycle {
    create_before_destroy = true
  }
}
