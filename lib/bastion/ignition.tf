# Systemd unit data resource containing the unit definition
data "ignition_systemd_unit" "etcd-member" {
  name = "etcd-member.service"
  enabled = true
}

data "template_file" "set-etcd-service" {
  template = "${file("${path.module}/templates/systemd/set-etcd.service.tmpl")}"

  vars {
    bastion_users = "${var.bastion_users}"
  }
}

data "ignition_systemd_unit" "set-etcd" {
  name    = "set-etcd.service"
  content = "${data.template_file.set-etcd-service.rendered}"
}

data "template_file" "ssh-bastion-service" {
  template = "${file("${path.module}/templates/systemd/ssh-bastion.service.tmpl")}"
}

data "ignition_systemd_unit" "ssh-bastion" {
  name = "ssh-bastion.service"
  content = "${data.template_file.ssh-bastion-service.rendered}"
}

data "ignition_config" "bastion" {
  systemd = [
    "${data.ignition_systemd_unit.etcd-member.id}",
    "${data.ignition_systemd_unit.set-etcd.id}",
    "${data.ignition_systemd_unit.ssh-bastion.id}",
  ]
}
