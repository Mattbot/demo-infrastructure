variable "environment" {}

variable "vpc_id" {}

variable "subnet_id" {}

variable key_name {}

variable "bastion_users" {}

variable "public_dns_zone_id" {
  description = "Route53 Hosted Zone ID for the automatic public DNS name and also the 'public_dns_name' (if supplied) for the service"
}

variable "public_dns_name" {
  description = "Record name for the 'public DNS name' within the Route53 Hosted Zone described by ${var.public_dns_zone_id}"
  default     = ""
}

