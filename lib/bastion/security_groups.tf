resource "aws_security_group" "ssh-bastion" {
  vpc_id = "${var.vpc_id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # SSH in from anywhere
  }

  ingress {
    from_port   = 34523
    to_port     = 34523
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # SSH in from anywhere
  }

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    self      = false
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Environment = "${var.environment}"
    Name        = "ssh-bastion"
    Paradigm    = "demo"
  }
}
