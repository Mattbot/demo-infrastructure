data "aws_ami" "ecs" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn-ami-*.f-amazon-ecs-optimized"] # search for AMZ linux, ECS-optimized images
  }

  filter {
    name   = "owner-alias"
    values = ["amazon"]    # search only for releases from Amazon
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
}

data "template_file" "cloudinit_ecs_config" {
  template = "${file("${path.module}/cloudinit/files/ecs.config.tmpl")}"

  vars {
    cluster_name = "${aws_ecs_cluster.main.name}"
  }
}

data "template_file" "cloudinit_script_init" {
  template = "${file("${path.module}/cloudinit/scripts/init.sh.tmpl")}"

  vars {
    hostname_domain = "${var.domain_name}"
  }
}

data "template_file" "cloudinit_cloudconfig" {
  template = "${file("${path.module}/cloudinit/cloud_config.yml.tmpl")}"

  vars {
    file_ecs_config = "${base64encode(data.template_file.cloudinit_ecs_config.rendered)}"
  }
}

data "template_cloudinit_config" "user_data" {
  gzip          = true
  base64_encode = true

  part {
    content_type = "text/cloud-config"
    content      = "${data.template_file.cloudinit_cloudconfig.rendered}"
  }

  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.cloudinit_script_init.rendered}"
  }
}

resource "aws_launch_configuration" "ecs_instance" {
  name_prefix          = "${var.environment}-${var.name}-ecs-instance-"
  image_id             = "${data.aws_ami.ecs.id}"
  instance_type        = "${var.ecs_instance_type}"
  iam_instance_profile = "${aws_iam_instance_profile.ecs_instance.arn}"
  key_name             = "${var.ssh_key_name}"                                  # TODO: define new SSH keys here in TF??
  security_groups      = ["${var.ecs_instance_security_groups}"]
  user_data            = "${data.template_cloudinit_config.user_data.rendered}"

  ebs_block_device {
    device_name = "/dev/xvdcz" # this volume is attached specifically to an lvm docker VG, not mounted normally
    volume_size = 22
    volume_type = "gp2"
  }

  lifecycle {
    create_before_destroy = true
  }
}
