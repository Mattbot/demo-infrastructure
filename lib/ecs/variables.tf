variable "environment" {
  description = "name of the environment"
}

variable "domain_name" {
  description = "FQDN of the domain this ECS-cluster owns"
}

variable "name" {
  description = "Name of the ECS cluster"
}

variable "ssh_key_name" {
  description = "name of preexisting EC2 SSH key"
}

variable "ecs_instance_type" {
  description = "EC2 instance type for the ECS instances"
}

variable "ecs_subnets" {
  type = "list"
}

variable "min_size" {}

variable "max_size" {}

variable "desired_capacity" {}

variable "vpc_name_from_environment" {
  description = "Logical name of the VPC defined in the environment, in which to build the cluster"
  default     = "vpc0"
}

variable "ecs_instance_security_groups" {
  type = "list"
}
