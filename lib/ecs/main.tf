data "terraform_remote_state" "environment" {
  backend = "s3"

  config {
    bucket = "net.mattbot.demo.terraform-state"
    key    = "environments/${var.environment}.tfstate"
  }
}
resource "aws_autoscaling_notification" "ecs" {
  topic_arn = "${aws_sns_topic.ecs_notifications.arn}"

  group_names = [
    "${aws_autoscaling_group.ecs.name}",
  ]

  notifications = [
    "autoscaling:EC2_INSTANCE_LAUNCH",
    "autoscaling:EC2_INSTANCE_LAUNCH_ERROR",
    "autoscaling:EC2_INSTANCE_TERMINATE",
  ]
}

resource "aws_sns_topic" "ecs_notifications" {
  name = "${var.environment}-${var.name}-ecs"
}

# resource "aws_sns_topic_subscription" "ecs_notifications" {
#   endpoint               = "https://mattbot-sns-slack-gateway.herokuapp.com/channel/ops-activity"
#   endpoint_auto_confirms = true
#   protocol               = "https"
#   topic_arn              = "${aws_sns_topic.ecs_notifications.arn}"
# }

resource "aws_ecs_cluster" "main" {
  name = "${var.environment}-${var.name}"
}

resource "aws_autoscaling_group" "ecs" {
  desired_capacity          = "${var.desired_capacity}"
  launch_configuration      = "${aws_launch_configuration.ecs_instance.name}"
  max_size                  = "${var.max_size}"
  min_size                  = "${var.min_size}"
  name                      = "${var.environment}-${var.name}-ecs-cluster"
  termination_policies      = ["OldestLaunchConfiguration"]
  vpc_zone_identifier       = ["${var.ecs_subnets}"]
  wait_for_capacity_timeout = "0"

  enabled_metrics = [
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupMaxSize",
    "GroupMinSize",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances",
  ]

  tag {
    key                 = "Paradigm"
    value               = "demo"
    propagate_at_launch = true
  }

  tag {
    key                 = "Cluster"
    value               = "${var.name}"
    propagate_at_launch = true
  }

  tag {
    key                 = "Environment"
    value               = "${var.environment}"
    propagate_at_launch = true
  }

  depends_on = ["aws_ecs_cluster.main"]

  lifecycle {
    ignore_changes = [
      "desired_capacity",
    ]
  }
}

resource "aws_autoscaling_policy" "add" {
  name                   = "${var.name}-add-instance"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 180
  autoscaling_group_name = "${aws_autoscaling_group.ecs.name}"
}

resource "aws_autoscaling_policy" "remove" {
  name                   = "${var.name}-remove-instance"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = "${aws_autoscaling_group.ecs.name}"
}

resource "aws_cloudwatch_metric_alarm" "cpu-res-high" {
  alarm_name          = "${var.name}-cpu-res-high"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUReservation"
  namespace           = "AWS/ECS"
  period              = "60"
  statistic           = "Average"
  threshold           = "60"
  alarm_description   = "This metric monitors ecs high cpu reservations"
  alarm_actions       = ["${aws_autoscaling_policy.add.arn}"]

  dimensions {
    ClusterName = "${aws_ecs_cluster.main.name}"
  }
}

resource "aws_cloudwatch_metric_alarm" "cpu-res-low" {
  alarm_name          = "${var.name}-cpu-res-low"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUReservation"
  namespace           = "AWS/ECS"
  period              = "60"
  statistic           = "Average"
  threshold           = "30"
  alarm_description   = "This metric monitors ecs low cpu reservations"
  alarm_actions       = ["${aws_autoscaling_policy.remove.arn}"]

  dimensions {
    ClusterName = "${aws_ecs_cluster.main.name}"
  }
}
