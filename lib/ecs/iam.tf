resource "aws_iam_instance_profile" "ecs_instance" {
  name_prefix = "ecs-instance-"
  role        = "${aws_iam_role.ecs_instance.name}"
}

resource "aws_iam_role" "ecs_instance" {
  name_prefix        = "ecs-instance-"
  path               = "${replace("/environments/${var.environment}/clusters/${var.name}/", "/[^a-zA-Z0-9/]+/", "")}"
  assume_role_policy = "${data.aws_iam_policy_document.ecs_instance_profile_assume.json}"
}

data "aws_iam_policy_document" "ecs_instance_profile_assume" {
  statement {
    actions = ["sts:AssumeRole"]

    principals = [
      {
        type        = "Service"
        identifiers = ["ec2.amazonaws.com"]
      },
    ]

    effect = "Allow"
  }
}

# attach the AWS-published default ECS policy for the ECS instance's role
resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = "${aws_iam_role.ecs_instance.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}
