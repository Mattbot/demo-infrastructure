data "aws_iam_policy_document" "service_role_assume" {
  statement {
    actions = ["sts:AssumeRole"]

    principals = [
      {
        type        = "Service"
        identifiers = ["ecs.amazonaws.com"]
      },
    ]

    effect = "Allow"
  }
}

resource "aws_iam_role" "service" {
  name               = "${var.app_name}-${var.name}.ecs-service"
  path               = "/apps/${replace(var.app_name, "/[^a-zA-Z0-9]+/", "/")}/"
  assume_role_policy = "${data.aws_iam_policy_document.service_role_assume.json}"
}

resource "aws_iam_role_policy_attachment" "service_ecs_default" {
  role       = "${aws_iam_role.service.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceRole"
}

data "aws_iam_policy_document" "task_role_assume" {
  statement {
    actions = ["sts:AssumeRole"]

    principals = [
      {
        type        = "Service"
        identifiers = ["ecs-tasks.amazonaws.com"]
      },
    ]

    effect = "Allow"
  }
}

resource "aws_iam_role" "task" {
  name               = "${var.app_name}-${var.name}.ecs-task"
  path               = "/apps/${replace(var.app_name, "/[^a-zA-Z0-9]+/", "/")}/"
  assume_role_policy = "${data.aws_iam_policy_document.task_role_assume.json}"
}

data "aws_iam_policy_document" "task_autoscaler_role_assume" {
  statement {
    actions = ["sts:AssumeRole"]

    principals = [
      {
        type        = "Service"
        identifiers = ["application-autoscaling.amazonaws.com"]
      },
    ]

    effect = "Allow"
  }
}

resource "aws_iam_role" "task_autoscaler" {
  name               = "${var.app_name}-${var.name}.ecs-task-autoscaler"
  path               = "/apps/${replace(var.app_name, "/[^a-zA-Z0-9]+/", "/")}/"
  assume_role_policy = "${data.aws_iam_policy_document.task_autoscaler_role_assume.json}"
}

resource "aws_iam_role_policy_attachment" "task_autoscaler_default" {
  role       = "${aws_iam_role.task_autoscaler.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceAutoscaleRole"
}
