output "iam_role_task_arn" {
  value = "${aws_iam_role.task.arn}"
}

output "iam_role_service_arn" {
  value = "${aws_iam_role.service.arn}"
}

output "iam_role_task_name" {
  value = "${aws_iam_role.task.name}"
}

output "iam_role_service_name" {
  value = "${aws_iam_role.service.name}"
}

output "fqdn" {
  value = "${aws_route53_record.canonical_hostname.fqdn}"
}

output "alb_dns_name" {
  value = "${aws_alb.web.dns_name}"
}

output "alb_dns_zone_id" {
  value = "${aws_alb.web.zone_id}"
}
