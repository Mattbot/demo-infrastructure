resource "aws_ecs_task_definition" "web" {
  family                = "${var.app_name}-${var.name}"
  container_definitions = "${var.container_definitions}"
  task_role_arn         = "${aws_iam_role.task.arn}"

  # default volume available for sharing sockets between app and nginx containers
  volume {
    name = "sockets"
  }
}

resource "aws_ecs_service" "web" {
  name                               = "${var.app_name}-${var.name}"
  cluster                            = "${var.ecs_cluster_id}"
  task_definition                    = "${aws_ecs_task_definition.web.arn}"
  desired_count                      = "${var.desired_count}"
  deployment_maximum_percent         = "${var.deployment_maximum_percent}"
  deployment_minimum_healthy_percent = "${var.deployment_minimum_healthy_percent}"
  iam_role                           = "${aws_iam_role.service.arn}"
  depends_on                         = ["aws_iam_role_policy_attachment.service_ecs_default"]

  load_balancer {
    target_group_arn = "${aws_alb_target_group.web.arn}"
    container_name   = "${var.elb_container_name}"
    container_port   = "${var.elb_container_port}"
  }

  lifecycle {
    prevent_destroy = true

    ignore_changes = [
      "desired_count",
    ]
  }
}

resource "aws_alb" "web" {
  name            = "${replace(var.app_name, "/(.{0,27}).*/", "$1")}${replace(sha1(var.app_name), "/(.{5}).*/", "$1")}" # name of ELB is limited to 32 characters; chop app_name down to 26 chars, add "-10ef3" (first 5 chars of SHA1)
  subnets         = ["${var.elb_subnets}"]
  security_groups = ["${var.elb_security_groups}"]

  # access_logs {
  #   bucket = "net.mattbot.demo.elb-access-logs"
  #   prefix = "${var.environment}/${var.cluster_name}/${var.app_name}"
  # }

  tags {
    Cluster     = "${var.cluster_name}"
    Application = "${var.app_name}"
    Product     = "${var.project_name}"
    Environment = "${var.environment}"
    Paradigm    = "demo"
  }

  lifecycle {
    ignore_changes = ["name"]
  }
}

resource "aws_alb_target_group" "web" {
  name                 = "${replace(var.app_name, "/(.{0,27}).*/", "$1")}${replace(sha1(var.app_name), "/(.{5}).*/", "$1")}" # name of target-group is limited to 32 characters; chop app_name down to 26 chars, add "10ef3" (first 5 chars of SHA1)
  port                 = 80
  protocol             = "HTTP"
  vpc_id               = "${var.vpc_id}"
  deregistration_delay = 30

  health_check {
    interval            = 120
    path                = "/health/status"
    port                = "traffic-port"
    healthy_threshold   = 2
    unhealthy_threshold = 3
  }

  tags {
    Cluster     = "${var.cluster_name}"
    Application = "${var.app_name}"
    Product     = "${var.project_name}"
    Environment = "${var.environment}"
    Paradigm    = "demo"
  }
}

resource "aws_alb_listener" "web-http" {
  count = "${var.elb_listen_80}"

  load_balancer_arn = "${aws_alb.web.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.web.arn}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "web-https" {
  load_balancer_arn = "${aws_alb.web.arn}"
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${var.elb_ssl_certificate_arn}"

  default_action {
    target_group_arn = "${aws_alb_target_group.web.arn}"
    type             = "forward"
  }
}

resource "aws_route53_record" "canonical_hostname" {
  zone_id = "${var.cluster_dns_zone_id}"
  name    = "${var.app_name}"
  type    = "A"

  alias {
    name                   = "${aws_alb.web.dns_name}"
    zone_id                = "${aws_alb.web.zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_appautoscaling_target" "target" {
  service_namespace  = "ecs"
  resource_id        = "service/${var.environment}-${var.cluster_name}/${var.app_name}-${var.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  role_arn           = "${aws_iam_role.task_autoscaler.arn}"
  min_capacity       = "${var.min_count}"
  max_capacity       = "${var.max_count}"
}

resource "aws_appautoscaling_policy" "add" {
  name               = "${aws_ecs_service.web.name}-add-tasks"
  service_namespace  = "ecs"
  resource_id        = "service/${var.environment}-${var.cluster_name}/${var.app_name}-${var.name}"
  scalable_dimension = "ecs:service:DesiredCount"

  adjustment_type         = "ChangeInCapacity"
  cooldown                = 60
  metric_aggregation_type = "Average"

  step_adjustment {
    metric_interval_lower_bound = 0
    scaling_adjustment          = 2
  }

  depends_on = ["aws_appautoscaling_target.target"]
}

resource "aws_appautoscaling_policy" "reduce" {
  name               = "${aws_ecs_service.web.name}-reduce-tasks"
  service_namespace  = "ecs"
  resource_id        = "service/${var.environment}-${var.cluster_name}/${var.app_name}-${var.name}"
  scalable_dimension = "ecs:service:DesiredCount"

  adjustment_type         = "ChangeInCapacity"
  cooldown                = 600
  metric_aggregation_type = "Average"

  step_adjustment {
    metric_interval_upper_bound = 0
    scaling_adjustment          = -1
  }

  depends_on = ["aws_appautoscaling_target.target"]
}

resource "aws_cloudwatch_metric_alarm" "cpu-util-high" {
  alarm_name          = "${aws_ecs_service.web.name}-cpu-util-high"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = "60"
  statistic           = "Average"
  threshold           = "2"
  alarm_description   = "This metric monitors ecs task high cpu utilization"
  alarm_actions       = ["${aws_appautoscaling_policy.add.arn}"]

  dimensions {
    ClusterName = "${var.environment}-${var.cluster_name}"
    ServiceName = "${aws_ecs_service.web.name}"
  }
}

resource "aws_cloudwatch_metric_alarm" "cpu-util-low" {
  alarm_name          = "${aws_ecs_service.web.name}-cpu-util-low"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = "600"
  statistic           = "Average"
  threshold           = "1"
  alarm_description   = "This metric monitors ecs task low cpu utilization"
  alarm_actions       = ["${aws_appautoscaling_policy.reduce.arn}"]

  dimensions {
    ClusterName = "${var.environment}-${var.cluster_name}"
    ServiceName = "${aws_ecs_service.web.name}"
  }
}

resource "aws_cloudwatch_metric_alarm" "healthy-hosts-low" {
  alarm_actions       = ["${var.notification_target_arns}"]
  alarm_description   = "Alarms when the ALB for ${var.environment}/${var.cluster_name}/${aws_ecs_service.web.name} has too few healthy hosts"
  alarm_name          = "${aws_ecs_service.web.name}-healthy-hosts-low"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "3"
  metric_name         = "HealthyHostCount"
  namespace           = "AWS/ApplicationELB"
  ok_actions          = ["${var.notification_target_arns}"]
  period              = "60"
  statistic           = "Minimum"
  threshold           = "${var.min_count}"
  treat_missing_data  = "breaching"

  dimensions {
    LoadBalancer = "${aws_alb.web.arn_suffix}"
    TargetGroup  = "${aws_alb_target_group.web.arn_suffix}"
  }
}
