variable "name" {
  description = "Name of the ecs-service (unique for the app only)"
  default     = "web"
}

variable "app_name" {
  description = "Name of the app to which this service belongs"
}

variable "environment" {
  description = "production|preproduction"
}

variable "project_name" {
  description = "Name of the code project this service's app comes from"
}

variable "cluster_name" {
  description = "Name of the 'nova-cluster'"
}

variable "ecs_cluster_id" {}

variable "desired_count" {
  description = "Number of tasks to run for the service"
  default     = "2"
}

variable "min_count" {
  description = "Minimun number of tasks to run for the service"
  default     = "2"
}

variable "max_count" {
  description = "Maximun number of tasks to run for the service"
  default     = "3"
}

variable "vpc_id" {}

variable "elb_container_port" {
  description = "Port on the elb_container_name to which to send ALB traffic"
  default     = "3000"
}

variable "elb_container_name" {
  description = "Name of the container within the container_definitions which will be served traffic by the ALB"
  default     = "web"
}

variable "elb_listen_80" {
  default     = "1"
  description = "whether to build an ELB listener for port 80 ('0' or '1')"
}

variable "elb_subnets" {
  description = "List of subnets into which to insert the service's ALB"
  type        = "list"
}

variable "elb_security_groups" {
  description = "List of security group IDs into which the ALB will exist"
  type        = "list"
}

variable "container_definitions" {
  description = "JSON formatted list of container definitions for the ECS task definition"
}

variable "cluster_dns_zone_id" {
  description = "ID of the Route53 DNS Zone into which the 'canonical' public DNS record (pointing to the ALB) will be created"
}

variable "elb_ssl_certificate_arn" {
  description = "ARN of the ACM certificate to attach to the ALB for the service"
}

variable "deployment_maximum_percent" {
  description = "The upper limit (as a percentage of the service's desiredCount) of the number of running tasks that can be running in a service during a deployment."
  default     = "200"
}

variable "deployment_minimum_healthy_percent" {
  description = "The lower limit (as a percentage of the service's desiredCount) of the number of running tasks that must remain running and healthy in a service during a deployment."
  default     = "50"
}

variable "notification_target_arns" {
  default     = []
  description = "ARN of target for human notifications about issues (for CloudWatch alarms, mostly)"
  type        = "list"
}
