variable "environment" {}

variable "name" {}

variable "region" {}

variable "availability_zones" {
  type = "list"
}

variable public_dns_zone_id {}

variable "roles" {
  default = {
    flow_logs = "arn:aws:iam::123445716356:role/flowlogsRole"
  }
}

variable "subnet_bitsize" {
  default = 8
}

variable "cidr" {
  default = "10.0.0.0/16"
}

variable "bastion_users" {}
