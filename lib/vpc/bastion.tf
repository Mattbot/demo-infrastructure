module "bastion" {
  source = "../../lib/bastion"

  environment             = "${var.environment}"
  vpc_id                  = "${aws_vpc.default.id}"
  subnet_id               = "${data.aws_subnet_ids.default.ids[0]}"
  public_dns_zone_id      = "${var.public_dns_zone_id}"
  key_name                = "demo"
  bastion_users              = "${var.bastion_users}"

}
