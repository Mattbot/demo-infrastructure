variable "environment" {}

variable "name" {}

variable "region" {
  default = "us-east-1"
}

variable "subnet_id" {}

variable "instance_type" {
  default = "t2.micro"
}

variable "ami" {}

variable "vpc_security_group_ids" {
  type = "list"
}

variable "key_name" {}

variable "user_data" {}
