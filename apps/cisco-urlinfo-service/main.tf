data "template_file" "container_definitions_web" {
  template = "${file("${path.module}/container_definitions/web.json")}"

  vars {
    aws_region            = "${var.region}"
    awslogs_group_name    = "${aws_cloudwatch_log_group.web.name}"
    awslogs_region        = "${var.region}"
    awslogs_stream_prefix = "/"
    docker_image          = "${var.docker_image_repo}:${var.docker_image_tag}"
    external_url          = "${coalesce("${var.external_url}", "${var.external_url_scheme}://${aws_route53_record.public_domain_name.fqdn}")}"
    gg_env                = "${var.deployment_name}"
    secret_key_base       = "${sha256("${var.environment}-${var.project_name}-${var.deployment_name}-${var.ecs_cluster_id}")}"

    app_name              = "${var.project_name}-${var.deployment_name}"
    database_url          = "${var.database_url}"
    hmac_secret           = "${var.hmac_secret}"
  }
}

module "service_web" {
  source = "../../lib/ecs-web-service"

  app_name                 = "${var.project_name}-${var.deployment_name}"
  cluster_dns_zone_id      = "${var.cluster_dns_zone_id}"
  cluster_name             = "${var.cluster_name}"
  container_definitions    = "${data.template_file.container_definitions_web.rendered}"
  desired_count            = "${var.desired_count}"
  min_count                = "${var.min_count}"
  max_count                = "${var.max_count}"
  ecs_cluster_id           = "${var.ecs_cluster_id}"
  elb_container_name       = "nginx"
  elb_container_port       = "80"
  elb_listen_80            = "0"
  elb_security_groups      = ["${var.elb_security_groups}"]
  elb_ssl_certificate_arn  = "arn:aws:acm:us-east-1:123445716356:certificate/3ed51a02-24a9-437f-afad-5abf974f054b"
  elb_subnets              = ["${var.elb_subnets}"]
  environment              = "${var.environment}"
  notification_target_arns = ["${var.notification_target_arns}"]
  project_name             = "${var.project_name}"
  vpc_id                   = "${var.vpc_id}"
}

# data "aws_iam_policy_document" "web_task" {
#   statement {
#     sid = "1"

#     actions = [
#       "ses:SendEmail",
#       "ses:SendRawEmail",
#     ]

#     resources = [
#       "arn:aws:ses:us-east-1:394453120661:identity/hello@mattbot.net",
#     ]
#   }
# }

# resource "aws_iam_policy" "web_task" {
#   name   = "${var.cluster_name}-${var.project_name}-${var.deployment_name}-web"
#   path   = "/apps/${replace("${var.project_name}/${var.deployment_name}", "/[^a-zA-Z0-9/]+/", "/")}/"
#   policy = "${data.aws_iam_policy_document.web_task.json}"
# }

# resource "aws_iam_role_policy_attachment" "web_task" {
#   role       = "${module.service_web.iam_role_task_name}"
#   policy_arn = "${aws_iam_policy.web_task.arn}"
# }

resource "aws_cloudwatch_log_group" "web" {
  name              = "/environments/${var.environment}/clusters/${var.cluster_name}/apps/${var.project_name}/${var.deployment_name}/services/web"
  retention_in_days = 30
}

# Always create a public DNS name on some supplied Zone ID. To add a "vanity" name, do so one level out as a CNAME/Alias to this record.
resource "aws_route53_record" "public_domain_name" {
  name    = "${var.project_name}-${var.deployment_name}"
  zone_id = "${var.public_dns_zone_id}"
  type    = "CNAME"
  ttl     = "180"
  records = ["${module.service_web.fqdn}"]
}
