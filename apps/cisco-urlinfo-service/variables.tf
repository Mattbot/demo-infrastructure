variable "database_url" {
  type        = "string"
  description = "Database connection URL"
}

variable "hmac_secret" {
  default = "secret_squirrel"
}
