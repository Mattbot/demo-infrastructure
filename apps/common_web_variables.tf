variable "elb_security_groups" {
  type = "list"
}

variable "elb_subnets" {
  type = "list"
}

variable "cluster_dns_zone_id" {
  description = "Route53 Hosted Zone ID for the 'Canonical DNS name' for the service"
}

variable "public_dns_zone_id" {
  description = "Route53 Hosted Zone ID for the automatic public DNS name and also the 'public_dns_name' (if supplied) for the service"
}

variable "public_dns_name" {
  description = "Record name for the 'public DNS name' within the Route53 Hosted Zone described by ${var.public_dns_zone_id}"
  default     = ""
}

variable "external_url" {
  description = "Externally available DNS name of the service (given to the container(s) so they may construct fully qualified URLs referencing themselves. Defaults to: `public_dns_name || canonical_hostname`"
  default     = ""
}

variable "external_url_scheme" {
  default     = "https"
  description = "When external_url is not set, the inferred values for external_url use this scheme. If external_url is set, this value is not used"
}
