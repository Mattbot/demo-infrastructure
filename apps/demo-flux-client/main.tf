data "template_file" "container_definitions_web" {
  template = "${file("${path.module}/container_definitions/web.json")}"

  vars {
    awslogs_group_name    = "${aws_cloudwatch_log_group.web.name}"
    awslogs_region        = "${var.region}"
    awslogs_stream_prefix = "/"
    docker_image          = "${var.docker_image_repo}:${var.docker_image_tag}"
    external_url          = "${coalesce("${var.external_url}", "${var.external_url_scheme}://${aws_route53_record.public_domain_name.fqdn}")}"
    env                   = "${var.deployment_name}"

    app_name              = "${var.project_name}-${var.deployment_name}"
    assets_host           = "${aws_route53_record.cloudfront.fqdn}"

    hugo_append_port      = "${var.hugo_append_port}"
    hugo_base_url         = "${var.hugo_base_url}"
    hugo_port             = "${var.hugo_port}"
  }
}

module "service_web" {
  source = "../../lib/ecs-web-service"

  app_name                 = "${var.project_name}-${var.deployment_name}"
  cluster_dns_zone_id      = "${var.cluster_dns_zone_id}"
  cluster_name             = "${var.cluster_name}"
  container_definitions    = "${data.template_file.container_definitions_web.rendered}"
  desired_count            = "${var.desired_count}"
  min_count                = "${var.min_count}"
  max_count                = "${var.max_count}"
  ecs_cluster_id           = "${var.ecs_cluster_id}"
  elb_container_name       = "nginx"
  elb_container_port       = "80"
  elb_security_groups      = ["${var.elb_security_groups}"]
  elb_ssl_certificate_arn  = "arn:aws:acm:us-east-1:123445716356:certificate/3ed51a02-24a9-437f-afad-5abf974f054b"
  elb_subnets              = ["${var.elb_subnets}"]
  environment              = "${var.environment}"
  notification_target_arns = ["${var.notification_target_arns}"]
  project_name             = "${var.project_name}"
  vpc_id                   = "${var.vpc_id}"
}

resource "aws_cloudwatch_log_group" "web" {
  name              = "/environments/${var.environment}/clusters/${var.cluster_name}/apps/${var.project_name}/${var.deployment_name}/services/web"
  retention_in_days = 30
}

# Always create a public DNS name on some supplied Zone ID. To add a "vanity" name, do so one level out as a CNAME/Alias to this record.
resource "aws_route53_record" "public_domain_name" {
  name    = "${var.project_name}-${var.deployment_name}"
  zone_id = "${var.public_dns_zone_id}"
  type    = "CNAME"
  ttl     = "180"
  records = ["${module.service_web.fqdn}"]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "cloudfront" {
  name    = "${var.project_name}-${var.deployment_name}-cdn"
  zone_id = "${var.public_dns_zone_id}"
  type    = "A"

  alias {
    zone_id                = "${aws_cloudfront_distribution.main.hosted_zone_id}"
    name                   = "${aws_cloudfront_distribution.main.domain_name}"
    evaluate_target_health = false                                                // this cannot be true for CloudFront aliases
  }
}

resource "aws_cloudfront_distribution" "main" {
  enabled     = true
  comment     = "${module.service_web.fqdn}"
  price_class = "PriceClass_All"
  aliases     = ["${replace(aws_route53_record.public_domain_name.fqdn, aws_route53_record.public_domain_name.name, "${aws_route53_record.public_domain_name.name}-cdn")}"]

  origin {
    domain_name = "${aws_route53_record.public_domain_name.fqdn}"
    origin_id   = "app"

    custom_origin_config {
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "https-only"                             # n.b. we're assuming the app supports SSL and the certificate it uses supports the auto-generated ${aws_route53_record.public_domain_name}
      origin_ssl_protocols   = ["SSLv3", "TLSv1", "TLSv1.1", "TLSv1.2"]
    }
  }

  logging_config {
    include_cookies = false
    bucket          = "cloud-front-logs.mattbot.net.s3.amazonaws.com"
    prefix          = "environments/${var.environment}/clusters/${var.cluster_name}/apps/${var.project_name}/${var.deployment_name}/"
  }

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "app"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn      = "arn:aws:acm:us-east-1:123445716356:certificate/3ed51a02-24a9-437f-afad-5abf974f054b"
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1"
  }

  lifecycle {
    create_before_destroy = true
  }
}
