variable "region" {}

variable "ecs_cluster_id" {}

variable "vpc_id" {}

variable "cluster_name" {
  type        = "string"
  description = "name of the cluster to deploy into"
}

variable "environment" {}

variable "project_name" {
  type        = "string"
  description = "Name of the project being deployed. Will have `deployment_name` appended to it to build the `app_name`"
}

variable "deployment_purpose" {
  description = "Short, token-style identifier of why this app is deployed (ideally can be used to tie distinct but related app instances together across app- or cluster-boundary. examples: 'staging', 'wercs-sandbox', 'production', 'demo', etc."
}

variable "deployment_name" {
  description = "Arbitrary but (ideally globally) unique identifier of this instance of this app"
}

variable "docker_image_tag" {
  description = "Docker image tag for app containers"
}

variable "docker_image_repo" {
  description = "Docker image repo for app containers"
}

variable "desired_count" {
  default     = 2
  description = "number of tasks to run of the service"
}

variable "min_count" {
  default     = 2
  description = "Minimun number of tasks to run of the service"
}

variable "max_count" {
  default     = 4
  description = "Maximum number of tasks to run of the service"
}

variable "notification_target_arns" {
  default     = []
  description = "ARN of target for human notifications about issues (for CloudWatch alarms, mostly)"
  type        = "list"
}
