output "fqdn" {
  value = "${aws_route53_record.public_domain_name.fqdn}"
}

output "log_groups" {
  value = "${map(
    "web",   "${aws_cloudwatch_log_group.web.name}",
  )}"
}
