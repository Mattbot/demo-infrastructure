variable "aws_account_id" {
  type        = "string"
  description = "AWS Account ID"
  default     = "123445716356"
}

variable "hugo_append_port" {
  default     = false
}
variable "hugo_base_url" {}
variable "hugo_port" {}
