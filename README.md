# Infrastructure as Code Demo

* Work in Progress *

## Introduction

This is [Terraform][terraform] based ["infrastructure as code"][infrastructure] for managing and deploying Docker and AWS Elastic Container Service based web application stacks and managing DNS records via Route 53.  Some of the benefits of this approach to infrastructure administration are:

- git is used to track changes
- changes are easier to examine, reason about, discuss, etc.
- auditing becomes easier (everything is defined in text files)
- patterns can be extracted into reusable modules
- less error prone than manual updates
- faster deployment of desired changes
- automation is trivial

This code is meant as a demonstration of the concept and is not particularly suited for use as deployment framework.  However, it is based off of a proven production infrastructure setup in current use by a high traffic web application and its supporting microservices.

## Features

This demo code manages the AWS infrastructure required to host auto-scalable, secure, multi-environment, micro-service based web applications including logging, monitoring, and dns record management. The design goal of this demo is realize the tennents of immutable architexture as fully as possible. The application micro-services run via Docker based container images. Application deployments are rolling, zero downtime updates. The clustered Docker host servers are lean AMI based images and require minimal provisioning, increasing the auto-scaling response time and reliability.

![Infrastructure as Code Map][IaCmap]

## Definitions

This are definitions specific to understanding this project and are not Terraform related per se:

- TF-root: A directory in which you run the `terraform` (or `tf`) command (other directories are "modules" or are otherwise used to organize the code).
- Environment: This corresponds to a Virtual Private Cloud (VPC) on AWS and is primarily used to isolate deployments into areas such as development, staging, and/or production.  Environments may contain multiple clusters.
- Bastion: Each environment includes an non-clustered Amazon Linux EC2 instance running an SSHd Docker container that you may proxy through to directly access the clustered instances by SSH. Their usage is documented in the [wiki][bastion].
- Cluster: This represents an Elastic Container Service (ECS) cluster of Amazon Linux EC2 instances configured as Docker hosts working in concert and managed by autoscaler.

## Setup

### AWS

You will need an [AWS][aws] account to deploy this project.

### Direnv

Please install [Direnv][direnv] to manage developer tool paths, environment variables, and your AWS credentials for this project.  An example [.envrc][envrc] is provided.

### Terraform

Terraform is run via a wrapper script which launches Terraform inside a Docker container to provide a consistant tool experience for the developer.  The docker server needs to be able to mount your local demo-infrastructure directory (a remote docker-machine will not work).

## Usage

### Workflow Tips

- Terraform's remote state doesn't work very nicely with the GIT branching model; work only in master branch when pushing code.
- Ensure your new Terraform code works and is deployed before checking it into the GIT master branch
- For Production environments, copy the modules in ```./lib``` and rename them with a ```.stable``` extension.

### DNS Management

### Web Application Deployment

## SSH Bastion Servers

Within each environment, there is an CoreOS EC2 Instance (the bastion) running a [ssh proxy server][dockerfile_ssh_proxy] on port 34523. This allows one to use ssh to connect to the bastion and proxy the ssh connection into the docker hosts in the ECS clusters within the environment's VPC. This is necessary to run Rails console tasks like data migrations.

Bastion users must have a Gitlab account with a public ssh key.  The bastion retrieves the users' public keys from gitlab.com and adds them to the bastion.  The users' names are set in the users.tf file for each environment.  (See the [preproduction environment][set_usernames] Terraform file for an example.)

Setup your local [ssh config][ssh_config_example] to use the bastion.  You will need your private Gitlab ssh key and your EC2 key pair pem file.

Once the above is complete, you may connect to an ECS instance via ssh:

```MATTBOT_USE_SSH_BASTION=1 ssh ec2-user@ec2-*-*-*-*.compute-1.amazonaws.com```

Should you need to connect directly to the CoreOS docker host, there is a convenience DNS entry for that:

```bash
# ssh core@${environment}-bastion-direct.mattbot.net
ssh core@preproduction-bastion-direct.mattbot.net
```

I recommend adding the following lines to your ```.aliases``` file:

```bash
# ssh to Mattbot bastion host:
alias mbssh='MATTBOT_USE_SSH_BASTION=1 ssh'
alias mbscp='MATTBOT_USE_SSH_BASTION=1 scp'
```

## Future Roadmap

The envisioned future features of this project include:

• Code generators that output the Terraform files for common tasks like the create of new environments.
• Integrating state change notifications and workflow commands into a Slack channel via a bot for better ops team visiability.

## Thanks

This approach was worked out in colaboration with my colleague [Ryan Long][ryan], one of the most talented engineers with whom I have the pleasure of working.

[aws]: https://aws.amazon.com
[bastion]: https://gitlab.com/Mattbot/demo-infrastructure/wikis/Using-the-SSH-Bastions
[terraform]: https://www.terraform.io
[direnv]: https://direnv.net/
[dockerfile_ssh_proxy]: https://gitlab.com/Mattbot/dockerfile-ssh-bastion
[envrc]: https://gitlab.com/Mattbot/demo-infrastructure/blob/master/.envrc.example
[ignition]: https://coreos.com/ignition/docs/latest/
[infrastructure]: https://en.wikipedia.org/wiki/Infrastructure_as_Code
[IaCmap]: https://gitlab.com/Mattbot/demo-infrastructure/raw/master/docs/images/InfrastructureAsCode.png "Infrastructure As Code Map"
[ryan]: https://rtlong.com
[set_usernames]: https://gitlab.com/Mattbot/demo-infrastructure/blob/master/environments/preproduction/users.tf
[ssh_config_example]: https://gitlab.com/Mattbot/demo-infrastructure/blob/master/docs/ssh/config.example
[ssh_proxy_service]: https://gitlab.com/Mattbot/dockerfile-ssh-bastion/blob/master/contrib/systemd/ssh-bastion.service





